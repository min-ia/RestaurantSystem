package controller;

import model.Employee;

import java.util.ArrayList;
import model.Client;

public class ControllerEmployee {
    
    private ArrayList<Employee> employeeList;
   
    public ControllerEmployee() throws Exception {
        this.employeeList = new ArrayList<>();
        Employee user = new Employee("admin", "000.000.000-00", "senha123");
        employeeList.add(user);
        user = new Employee("Jorge Amado", "100.000.000-11", "senha123");
        employeeList.add(user);
        user = new Employee("Maurício da Silva", "200.000.000-22", "senha123");
        employeeList.add(user);
    }
    
    public ArrayList<Employee> getListEmployee() {
        return employeeList;
    }
    
    public void add(Employee employee) throws Exception {
        boolean flag = true;
        for(Employee e : employeeList) {
            if(employee.getCpf().equals(e.getCpf())){
                flag = false;
                break;
            }
        }
        
        if(flag) {
             employeeList.add(employee);
        }
        else {
            throw new Exception("CPF já cadastrado.");
        }
    }
    
    public void delete(Employee employee) {
        employeeList.remove(employee);
    }
    
    public Employee get(int n) {
        return employeeList.get(n);
    }
}
