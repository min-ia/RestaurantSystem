package controller;

import model.Client;

import java.util.ArrayList;

public class ControllerClient {
    
    private ArrayList<Client> clientList;
   
    public ControllerClient() throws Exception {
        this.clientList = new ArrayList<>();
        Client c = new Client("José da Silva", "123.456.789-00");
        this.clientList.add(c);
        c = new Client("Maria Joaquina", "223.456.789-00");
        this.clientList.add(c);
        c = new Client("Camila Souza", "323.456.789-00");
        this.clientList.add(c);
    }
    
    public ArrayList<Client> getListClient() {
        return clientList;
    }
    
    public void add(Client client) throws Exception {
        boolean flag = true;
        for(Client c : clientList) {
            if(client.getCpf().equals(c.getCpf())){
                flag = false;
                break;
            }
        }
        
        if(flag) {
            clientList.add(client);
        }
        else {
            throw new Exception("CPF já cadastrado.");
        }
    }
    
    public void delete(Client client) {
        clientList.remove(client);
    }
    
    public Client get(int n) {
        return clientList.get(n);
    }
}
