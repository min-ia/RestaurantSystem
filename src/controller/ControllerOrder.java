package controller;

import model.Order;

import java.util.ArrayList;
import model.Product;

public class ControllerOrder {
    
    private ArrayList<Order> orderList;
   
    public ControllerOrder() throws Exception {
        this.orderList = new ArrayList<>();
        Product p = new Product("Salada", "10.5");
    }
    
    public ArrayList<Order> getListOrder() {
        return orderList;
    }
    
    public void add(Order order) {
        orderList.add(order);
    }
    
    public void delete(Order order) {
        orderList.remove(order);
    }
    
    public boolean verifyOpenOrder() {
        for(Order o : orderList){
            if(o.getStatus() == 1){
                return true;
            }
        }
        
        return false;
    }
}
