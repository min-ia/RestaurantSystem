package model;

public class Employee extends Person{
	
	private String password;
	
	public Employee(String name, String cpf, String password) throws Exception {
		setName(name);
		setCpf(cpf);
		setPassword(password);
	}

        public Employee() {
        }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		
		if(password != null && !password.isEmpty() && !password.trim().isEmpty()) {
			
			if(password.length() > 7){
				this.password = password;
			}
			else {
				throw new IllegalArgumentException("Senha muito curta.");
			}
			
		}
		else {
			throw new IllegalArgumentException("Senha Inválida.");
		}
	}
	
}
