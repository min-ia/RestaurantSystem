/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author iasmin
 */
public class Stock {
    
    Product product;
    int mininum;
    int amount;
    
     public Stock(Product product, int mininum, int amount) throws Exception {
        setProduct(product);
        setMininum(mininum);
        setAmount(amount);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getMininum() {
        return mininum;
    }

    public void setMininum(int mininum) throws Exception {
        if(mininum >= 0){
            this.mininum = mininum;
        } else {
            throw new Exception("Valor inválido para estoque.");
        }
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) throws Exception {
        if(amount >= 0){
            this.amount = amount;
        } else {
            throw new Exception("Valor inválido para estoque.");
        }
    }
    
    public void removeStock(int amount) throws Exception {
        if(this.amount >= amount){
            this.amount -= amount;
        } else {
            throw new Exception("Não há estoque suficiente deste produto.");
        }
    }
    
    public void addStock(int amount) throws Exception {
        this.amount += amount;       
    }
    
    public String toString() {
        return getProduct().getName();
    }

}
