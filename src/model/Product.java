/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author iasmin
 */
public class Product {
    
    private String name;
    private Float price;
    
    public Product(String name, String price) throws Exception {
        setName(name);
        setPrice(price);
    }
    
    public void setName(String name) throws Exception {
       if(name != null && !name.isEmpty() && !name.trim().isEmpty()) {
            this.name = name;
        }
        else {
            throw new Exception("Nome Inválido.");
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setPrice(String price) throws Exception {
         if(price != null && !price.isEmpty() && !price.trim().isEmpty()) {
            price = price.replace(",", ".");
            
            if(Float.parseFloat(price) >= 0) {
                this.price = Float.parseFloat(price);
            }
            else {
                throw new Exception("Preço Inválido.");
            }
        }
        else {
            throw new Exception("Preço Inválido.");
        }
    }
    
    public Float getPrice() {
        return this.price;
    }
    
    public String toString() {
        return getName();
    }
    
}
