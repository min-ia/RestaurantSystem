package model;

public class Payment {
    
    private ServiceOrder os;
    private float money = 0;
    private PayCard payCard;

    public Payment() {}
    
    public Payment(ServiceOrder os) throws Exception {
        if(os != null){
            this.os = os;
            this.money = 0;
            payCard = new PayCard();
        } else {
            throw new Exception("Pagamento Inválido.");
        }
    }

    public ServiceOrder getOs() {
        return os;
    }

    public void setOs(ServiceOrder os) {
        this.os = os;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public PayCard getPayCard() {
        return payCard;
    }

    public void setPayCard() {
        this.payCard.setValue(os.amountPay() - money);
    }
    
    public float returnPayment() {
        return money + payCard.getInstallmentsValue()*payCard.getNumber_installments() - os.amountPay();
    }
    
    public float valueCard() {
        return os.amountPay() - money;
    }
    
    public boolean verifyPayment() {
        return returnPayment() >= 0;
    }
   
}
